from elasticsearch import Elasticsearch, helpers

SERVERS = ["cats1-13.it.census.gov:9200"]
#SERVERS = ["localhost:9200"]

OLD_GEO_INDEX = "geo-index"

NEW_GEO_INDEX = "merge-geo"
NEW_GEO_TYPE = "metadata"
NEW_GEO_SETTINGS = {
	"settings": {
	    "index": {
	        "analysis": {
				"tokenizer" : {
					"typeahead_tokenizer" : {
					"type": "ngram",
					"min_gram": 1,
					"max_gram": 30,
					"token_char": ["letter", "digit", "whitespace"]
					}
				},
				"filter":{},
				"analyzer": {
					"standard_mod": {
						"type": "custom",
						"tokenizer": "standard",
						"filter": ["lowercase"]
					},
					"typeahead": {
						"type" : "custom",
						"tokenizer": "typeahead_tokenizer",
						"filter": ["lowercase"]
					}
				}
			},
			"number_of_replicas": "0",
			"number_of_shards": "3",
			"refresh_interval": "-1"
		}
	},
	"mappings": {
		 "metadata": {
			"properties": {
			   "name": {
					"type": "string",
					"analyzer": "standard_mod",
					"copy_to": "n_gram_name",
					"fields": {
						"raw": {
							"type": "string",
							"index": "not_analyzed",
							"doc_values": "true"
						}
					}
			   },
			   "n_gram_name": {
					"type": "string",
					"analyzer": "typeahead"
				},
			   "summaryLevel": {
				  	"type": "string",
				  	"index": "not_analyzed"
			   },
			   "all_geocodes": {
				  	"type": "string",
				  	"index": "not_analyzed"
			   },
			   "all_within" : {
			   		"type": "string",
			   		"index": "not_analyzed"
			   },
			   "2014" : {
					"properties": {
						"within": {
							"type": "string",
							"index": "not_analyzed"
						},	 
						"geocode": {
							"type": "string",
							"index": "not_analyzed"
						}
					}
				},
				 "2013" : {
					"properties": {
						"within": {
							"type": "string",
							"index": "not_analyzed"
						},
						"geocode": {
							"type": "string",
							"index": "not_analyzed"
						}
					}
				},
				"2012" : {
					"properties": {
						"within": {
							"type": "string",
							"index": "not_analyzed"
						},
						"geocode": {
							"type": "string",
							"index": "not_analyzed"
						}
					}
				}		               
			}
		}
	}
}

es = Elasticsearch(SERVERS)

if es.indices.exists(index=NEW_GEO_INDEX):
	print "DELETE:", NEW_GEO_INDEX, es.indices.delete(index=NEW_GEO_INDEX)

print "CREATE:", NEW_GEO_INDEX, es.indices.create(index=NEW_GEO_INDEX, ignore=400, body=NEW_GEO_SETTINGS)

docs = dict() # docs to ingest

batch_num = 0
old_docs_processed = 0
scroll = es.search(index=OLD_GEO_INDEX, scroll="360s", search_type="scan", size=100)
scroll_id = scroll["_scroll_id"]

while True:
	result = es.scroll(scroll_id=scroll_id, scroll="360s")

	hits = result["hits"]["hits"]
	scroll_len = len(hits)
	if not scroll_len > 0: break

	for hit in hits:
		source = hit["_source"]

		name = source["name"]
		summaryLevel = source["summarylevel"][1]
		key = (name, summaryLevel)

		doc = None

		if key in docs:
			doc = docs[key]
		else:
			doc = { "name": name, "summaryLevel": summaryLevel, "_index": NEW_GEO_INDEX, "_type": NEW_GEO_TYPE, "all_geocodes": set(), "all_within": set() }

		vintage = source["vintage"][1]
		geocode = source["geocode"][0]
		doc["all_geocodes"].add(geocode)
		doc[vintage] = { "geocode": geocode }
		if "parents" in source["within"]:
			within = source["within"]["parents"] 
			doc[vintage]["within"] = within
			for parent in within: doc["all_within"].add(parent)
		else:
			doc[vintage]["within"] = {}

		docs[key] = doc

	scroll_id = result["_scroll_id"]
	old_docs_processed += scroll_len
	batch_num += 1
	print "PROCESSED: Batch", batch_num, "of size", scroll_len

for doc in docs.values(): 
	doc["all_within"] = list(doc["all_within"])
	doc["all_geocodes"] = list(doc["all_geocodes"])

print "TOTAL OLD DOCS PROCESSED:", old_docs_processed
print "NUM DOCS TO INGEST:", len(docs)

helpers.bulk(client = es, actions = docs.itervalues(), chunk_size = 500, request_timeout = 100)
es.indices.refresh(index=NEW_GEO_INDEX)

print "DONE"