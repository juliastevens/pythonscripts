import json
from elasticsearch import Elasticsearch, helpers

SERVERS = ['localhost:9200']
INDEX = 'naics'
TYPE = 'geos'

def naicsToDictionary(): 
	f = open('/Users/juliastevens/Documents/CBP_2012_CB1200A12.txt', 'r')
	output = {}

	for line in f:
		line_json = json.loads(line)
		naicsID = line_json['naicsID']
		geoID = line_json['geoID']
		tid = line_json['tid']
		sumLevel = geoID[:3]

		key = (naicsID, sumLevel, tid)
		if key in output: 
			output[key].append(geoID)
		else: 
			output[key] = [geoID]
	
	f.close()
	return output

def ingestDictionary(data): 
	es = Elasticsearch(SERVERS)

	SETTINGS = {
		"settings": {
			"mappings": {
				 "metadata": {
					"properties": {
					   "naicsCode": {
							"type": "string"
					   },
					   "geoIDs": {
							"type": "string",
							"index": "not_analyzed"
						},
					   "summaryLevel": {
						  "type": "string",
						  "index": "not_analyzed",
					   },
					   "tableID": {
						  "type": "string",
						  "index": "not_analyzed"
					   }
					}
				}
			}
		}
	}

	if es.indices.exists(index=INDEX):
		print "DELETE:", INDEX, es.indices.delete(index=INDEX)

	print "CREATE:", INDEX, es.indices.create(index=INDEX, ignore=400, body=SETTINGS)

	docs = {}

	for key in data: 
		naicsID = str(key[0])
		sumLevel = str(key[1])
		tid = str(key[2])
		geoIDs = data.get(key)

		docs[key] = {"naicsCode": naicsID, "summaryLevel": sumLevel, "tableID": tid, "geoIDs": geoIDs, "_index": INDEX, "_type": TYPE}

	helpers.bulk(es, docs.itervalues())	


naicsData = naicsToDictionary()
ingestDictionary(naicsData)