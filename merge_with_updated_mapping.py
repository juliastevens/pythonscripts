import sys
import time
import logging
import re

logging.basicConfig()

from elasticsearch import Elasticsearch

#Set variables
servers = ['localhost:9200']
es = Elasticsearch(servers,timeout=120)
INDEX_NAME = "merge-geo"
TYPE = "metadata"

def scrollES( ind ):
    rs = es.search(
           index=[ind],
           scroll='360s',
           search_type='scan',
           size=100,
           timeout="20m", 
           body="{\"query\": {\"match_all\": {}}}"
    )
    return rs;

def populate2014():
	rs = scrollES('newgeo-2014')
	scroll_size = rs['hits']['total']
	num_records = scroll_size
	
	# index settings
	settings = {
		"settings": {
		    "index": {
		        "analysis": {
					"tokenizer" : {
						"typeahead_tokenizer" : {
						"type": "ngram",
						"min_gram": 1,
						"max_gram": 30,
						"token_char": ["letter", "digit", "whitespace"]
						}
					},
					"filter":{},
					"analyzer": {
						"standard_mod": {
							"type": "custom",
							"tokenizer": "standard",
							"filter": ["lowercase"]
						},
						"typeahead": {
							"type" : "custom",
							"tokenizer": "typeahead_tokenizer",
							"filter": ["lowercase"]
						}
					}
				},
				"number_of_replicas": "0",
				"number_of_shards": "3",
				"refresh_interval": "-1"
			}
		},
		"mappings": {
			 "metadata": {
				"properties": {
				   "name": {
						"type": "string",
						"analyzer": "standard_mod",
						"copy_to": "geoname",
						"include_in_all": "false",
						"fields": {
							"raw": {
								"type": "string",
								"index": "not_analyzed",
								"doc_values": "true",
								"include_in_all": "false"
							}
						}
				   },
				   "geoname": {
						"type": "string",
						"analyzer": "typeahead",
						"include_in_all": "false"
					},
				   "summarylevel": {
					  "type": "string",
					  "index": "not_analyzed",
					  "include_in_all": "false"
				   },
				   "most_recent": {
					  "type": "string",
					  "index": "not_analyzed",
					  "include_in_all": "false"
				   },
				   "2014" : {
						"properties": {
								"within": {
									"properties": {
										"parents": {
											"type": "string",
											"index": "not_analyzed",
											"include_in_all": "true"
										},
										"summarylevel": {
											"type": "string",
											"index": "not_analyzed",
											"include_in_all": "false"
										}
									}
								}, 
							"geocode": {
								"type": "string",
								"index": "not_analyzed",
								"include_in_all": "false"
							}
						}
					},
					 "2013" : {
						"properties": {
							"within": {
								"properties": {
									"parents": {
										"type": "string",
										"index": "not_analyzed",
										"include_in_all": "true"
									},
									"summarylevel": {
										"type": "string",
										"index": "not_analyzed",
										"include_in_all": "false"
									}
								}
							}, 
							"geocode": {
								"type": "string",
								"index": "not_analyzed",
								"include_in_all": "false"
							}
						}
					},
					"2012" : {
						"properties": {
							"within": {
								"properties": {
									"parents": {
										"type": "string",
										"index": "not_analyzed",
										"include_in_all": "true"
									},
									"summarylevel": {
										"type": "string",
										"index": "not_analyzed",
										"include_in_all": "false"
									}
								}
							}, 
							"geocode": {
								"type": "string",
								"index": "not_analyzed",
								"include_in_all": "false"
							}
						}
					}		               
				}
			 }
		  }
	}
	if es.indices.exists(index = INDEX_NAME):
		es.indices.delete(index= INDEX_NAME)
	# create index
	es.indices.create(index=INDEX_NAME, ignore=400, body = settings)
	count = 0
	othcount = 0

	while (scroll_size > 0): 
		try: 
			geos = []
			fieldsdata = {}
			scroll_id = rs['_scroll_id']
			rs = es.scroll(scroll_id = scroll_id, scroll = '360s')
			geos = rs['hits']['hits']
			scroll_size = len(rs['hits']['hits'])

			othcount = othcount + len(geos)
			print "Scroll size " + str(scroll_size)

			for geo in geos: 
				count = count + 1
				geocode = geo['_source']['geocode']
				geocode = geocode[0]
				name = geo['_source']['name']
				parents = geo['_source']['within']['parents']
				within_summary = geo['_source']['within']['summarylevel']
				summarylevel = geo['_source']['summarylevel']
				
				for lev in summarylevel : 
					if lev != "*":
						summarylevel = lev
				if summarylevel == "040":
					print str(name)

				json = {}
				json['name'] = name
				json['summarylevel'] = summarylevel
				json['most_recent'] = "2014"
				json['2014'] = {"within": {"parents": parents , "summarylevel": within_summary}, "geocode": geocode}
				data = es.index(index=INDEX_NAME, doc_type=TYPE, timeout="20000", body=json, op_type='create')

				if (data['created'] is False):
					print "False creation of " + str(name) + " " + str(geocode)
		except:
			print "Error indexing document " + str(name) + "  " + str(geocode)
			pass
	es.indices.refresh(index=INDEX_NAME)
	print str(count) + " documents indexed"
	print str(othcount) + " documents gone through"



def mergeAdditionalYears(years):
	# 2014 is already added, this method adds any additional years and merges them appropriately.
	for year in years: 
		rs = scrollES('newgeo-' + str(year))
		scroll_size = rs['hits']['total']
		num_records = scroll_size

		while (scroll_size > 0): 
			try: 
				geos = []
				fieldsdata = {}
				scroll_id = rs['_scroll_id']
				rs = es.scroll(scroll_id = scroll_id, scroll = '360s')
				geos = rs['hits']['hits']
				scroll_size = len(rs['hits']['hits'])

				# Gets every doc from the newgeo- index of the appropriate year.

				for geo in geos: 
					#Stores each field of the doc for given year
					name = geo['_source']['name']
					parents = geo['_source']['within']['parents']
					within_summary = geo['_source']['within']['summarylevel']
					summarylevel = geo['_source']['summarylevel']
					geocode = geo['_source']['geocode']
					geocode = geocode[0]
					
					# Gets rid of * summary level
					for lev in summarylevel : 
						if lev != "*":
							summarylevel = lev

					# Finds matching document in the new merge-geo index and checks for name and summary level equivalence. 
					match_name = {"query": {"filtered": {"filter": {"term": {"name.raw": name}}}}}
					match = es.search(index = INDEX_NAME, doc_type = TYPE, body = match_name)

					# If there are documents with the same name, go through and check if the summary level matches as well.
					if (len(match['hits']['hits']) > 0) :
						for doc in match['hits']['hits']:
							docSum = doc['_source']['summarylevel']

							# If summary level matches, add information into current document
							if (docSum == summarylevel): 
								body = { "doc" : {str(year): {"geocode" : geocode , "within": {"summarylevel": within_summary, "parents": parents}}}}
								up = es.update(index = INDEX_NAME, doc_type = TYPE, id = doc['_id'], body = body)
							# If summary level does not match, create a new document for this name and summary level.
							elif (docSum == "e60"): 
								pass
							else :
								json = {}
								json['name'] = name
								json['summarylevel'] = summarylevel
								json['most_recent'] = str(year)
								json[str(year)] = {"within": {"parents": parents , "summarylevel": within_summary}, "geocode": geocode}
								es.index(index=INDEX_NAME, doc_type=TYPE, timeout="20000", body=json)
					# If there is not yet a document with that name in the index, create it!
					else :
						json = {}
						json['name'] = name
						json['summarylevel'] = summarylevel
						json['most_recent'] = str(year)
						json[str(year)] = {"within": {"parents": parents , "summarylevel": within_summary}, "geocode": geocode}
						es.index(index=INDEX_NAME, doc_type=TYPE, timeout="20000", body=json)
			except:
				print "Error indexing document " + str(name) + "  " + str(geocode)
				pass
		es.indices.refresh(index=INDEX_NAME)

# No longer used with new most_recent format. 
def addMostRecent():
	rs = scrollES(INDEX_NAME)
	scroll_size = rs['hits']['total']
	num_records = scroll_size
	while (scroll_size > 0): 
		try: 
			geos = []
			fieldsdata = {}
			scroll_id = rs['_scroll_id']
			rs = es.scroll(scroll_id = scroll_id, scroll = '360s')
			geos = rs['hits']['hits']
			scroll_size = len(rs['hits']['hits'])

			for geo in geos: 
				most_recent = 1000
				geocode = geo['_source']['geocode']
				for year in geocode:
					if int(year) > most_recent:
						most_recent = year

				geocode['most_recent'] = geocode[str(most_recent)]

				body = { "doc" : {"geocode" : geocode}}
				#es.update(index = "merge-geo", doc_type = "metadata", id = geo['_id'], body = body)
		except: 
			pass


populate2014()
print "2014 added successfully"
#mergeAdditionalYears([2012,2013])

print ("Merge Completed")

exit